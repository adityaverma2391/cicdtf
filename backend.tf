terraform {
  backend "s3" {
    bucket = "s3statebackend231291"
    key    = "state"
    region = "us-east-1"
    dynamodb_table = "state-lock"
  }
}