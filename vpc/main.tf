#1 vpc, 1 subnet and security

resource "aws_vpc" "my_vpc" {
  cidr_block = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support = true

  tags = {
    name = "vpc-1"
  }
}

resource "aws_subnet" "pb_sn" {
  vpc_id = aws_vpc.my_vpc.id
  cidr_block = var.pub_cidr
  map_public_ip_on_launch = true
  availability_zone = "us-east-1a"

  tags = {
    name = "pb_sn1"
  }
}

resource "aws_security_group" "sg" {
  vpc_id = aws_vpc.my_vpc.id
  name = "my_sg"
  description = "Open 22,443,80,8080"

#Inbound traffic
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [var.ext_ip]
  }
#Outbound traffic
  egress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = [var.ext_ip]
  }
}