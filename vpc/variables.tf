variable "vpc_cidr" {
  type = string
}

variable "pub_cidr" {
  type = string
}

variable "ext_ip" {
  type = string
}