#!/bin/bash

exec > >(tee -i /var/log/user-data.log) #It will generate a file with user-data.log

exec 2>&1

sudo apt update -y
sudo apt install software-properties-common
sudo add-apt-repository --yes --update ppa:ansible/ansible
sudo apt install ansible -y
sudo apt install git -y

mkdir Ansible && cd Ansible

pwd
# Clone Repository
git clone https://github.com/adityaverma2391/ANSIBLE.git

cd ANSIBLE

ansible-playbook -i localhost Jenkins-playbook.yml
