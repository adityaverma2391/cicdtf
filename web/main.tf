#ec2
resource "aws_instance" "server" {
  ami = "ami-080e1f13689e07408"
  instance_type = "t2.micro"
  subnet_id = var.sn
  security_groups = [var.sg]
  user_data = file("./install_jenkins.sh")
  key_name = "ec2-key-aditya"

  tags = {
    name = "JenkinsServer"
  }
}